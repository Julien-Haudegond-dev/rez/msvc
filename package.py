name = "msvc"

version = "14.33.31629"

variants = [
    ["platform-windows", "arch-AMD64"],
]

build_command = ""

with scope("config") as c:
    c.release_packages_path = r"D:\rez\pkgs\ext"


def commands():
    interesting_names = ["Path", "LIB", "LIBPATH", "INCLUDE"]
    variables = this._vcvars

    for name in interesting_names:
        value = variables[name]

        if name == "Path":
            name = "PATH"

        if isinstance(value, list):
            for item in value:
                env[name].append(item)
        else:
            env[name].set(value)


@early()
def _vcvars():
    import os
    import re
    import subprocess

    def get_env_from_cmd(cmd):
        process = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = process.communicate()
        out = out.decode("utf-8").splitlines()

        env = {}

        pattern = r"(?P<name>\w+)=(?P<value>.*)"
        for line in out:
            match = re.match(pattern, line)
            if not match:
                continue

            name = match.group("name")
            value = match.group("value")
            
            if ";" in value:
                value = value.split(";")
            
            env[name] = value

        return env

    def get_env_diff(env_before, env_after):
        env_diff = {}
        for name, value_after in env_after.items():
            value_before = env_before.get(name, None)

            if value_before is None:
                env_diff[name] = value_after
                continue

            if value_after == value_before:
                continue

            if isinstance(value_after, str):
                # This is the new value that should be set to the variable.
                env_diff[name] = value_after
                continue

            if isinstance(value_after, list):
                if not isinstance(value_before, list):
                    # In case the old value was a simple string, we cast it to list for comparisons.
                    value_before = [value_before]
                
                diff = list(set(value_after) - set(value_before))
                env_diff[name] = diff
                continue

            raise RuntimeError("This error should not be raised. There was an issue during the environment diff check.")

        return env_diff

    vcvars_bat = os.path.join(
        "C:" + os.sep,
        "Program Files",
        "Microsoft Visual Studio",
        "2022",
        "Community",
        "VC",
        "Auxiliary",
        "Build",
        "vcvars64.bat",
    )
    cmd_bat = f'call ^"{vcvars_bat}^" && set'  # Escape quotes (for accurate use in cmd).

    # Get the difference between an environment without VCVars and with.
    before = get_env_from_cmd("set")
    after = get_env_from_cmd(cmd_bat)

    return get_env_diff(before, after)
