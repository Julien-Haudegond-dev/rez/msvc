# Rez Package - MSVC (for Windows)

Extract useful variables from `vcvars64.bat`. 
See Microsoft doc [here](https://learn.microsoft.com/fr-fr/cpp/build/building-on-the-command-line?view=msvc-170#path_and_environment).